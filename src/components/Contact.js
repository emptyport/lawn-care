import React from 'react'

const Contact = () => (
  <div id="contact" className="item">
    <h1 css={{color:'white'}}>Contact</h1>
    <p>
      Call us at 555-555-5555 for a free consultation today!
    </p>
  </div>
)

export default Contact