import React from 'react'
import Img from 'gatsby-image'

const Banner = (props) => (
  <div css={{
    position: 'relative',
    
  }}>
    <Img fluid={props.imgSrc} css={{
      "&:after": {
        backgroundImage: 'linear-gradient(to bottom, rgba(255, 255, 255, 0) 0, #fff 100%)',
        content: ''
      }
    }}/>
    <div css={{
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      textAlign: 'center',
      color: 'white',
      fontSize: '24px',
      padding: '0.5rem',
      display: 'inline',
      fontWeight: 'bold',
      lineHeight: '1.7em'
    }}>
      Providing quality lawn care at an affordable price
    </div>
  </div>
)

export default Banner