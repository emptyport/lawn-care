import React from 'react'

const Services = () => (
  <div id="services" className="item">
    <h1 css={{color:'white'}}>Services we offer</h1>
    <ul>
      <li>Mowing</li>
      <li>Edging</li>
      <li>Weeding</li>
      <li>Hedge trimming</li>
      <li>Gutter cleaning</li>
    </ul>
  </div>
)

export default Services