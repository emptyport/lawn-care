import React from 'react'

const Card = (props) => (
    <div className="card">
      <h2 className="card-title">{props.cardTitle}</h2>
      <h3 className="card-price">{props.cardPrice}</h3>
      <p className="card-description">{props.cardDescription}</p>
    </div>

)

export default Card