import React from 'react'
import Card from './Card'

const Pricing = () => (
  <div id="pricing" className="item">
    <h1 css={{color:'white'}}>Pricing</h1>

    <div className="card-container">
      <Card cardTitle="Standard" cardPrice="$25 /week" cardDescription="Mowing plus trimming/edging"/>
      <Card cardTitle="Deluxe" cardPrice="$35 /week" cardDescription="Everything in the Standard package plus de-weeding"/>
      <Card cardTitle="Master" cardPrice="$50 /week" cardDescription="Everything in the Standard and Deluxe packages plus hedge trimming and gutter cleaning"/>

    </div>

  </div>
)

export default Pricing