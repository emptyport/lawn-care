import React from 'react'

const Footer = () => (
  <div css={{
    background: '#000000aa',
    width: '100%',
    display: 'inline-block'
  }}>
    <span>Designed by Michael Porter</span> <span css={{
      float: 'right'
    }}>Copyright 2018</span>
  </div>
)

export default Footer