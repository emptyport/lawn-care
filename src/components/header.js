import React from 'react'
import { Link } from 'gatsby'

const Header = ({ siteTitle }) => (
  <div css={{
    display: 'grid',
    gridTemplateColumns: '3fr 1fr 1fr 1fr',
    color: 'white',
    position: 'fixed',
    top: '0',
    width: '100%',
    zIndex: '100',
    textDecoration: 'none',
    background: '#000000aa'
  }}>
    <Link to='/' css={{
      textDecoration: 'none',
      color: 'white',
      boxShadow: '0 0px 0 0 currentColor',
      fontWeight: 'bold'
    }}>Lawn Care Professionals</Link>
    
    <Link to='#services' css={{
      justifySelf: 'center',
      textDecoration: 'none',
      color: 'white',
      boxShadow: '0 0px 0 0 currentColor'
    }}>Services</Link>
    
    <Link to='#pricing' css={{
      justifySelf: 'center',
      textDecoration: 'none',
      color: 'white',
      boxShadow: '0 0px 0 0 currentColor'
    }}>Pricing</Link>

    <Link to='#contact' css={{
      justifySelf: 'center',
      textDecoration: 'none',
      color: 'white',
      boxShadow: '0 0px 0 0 currentColor'
    }}>Contact</Link>

  </div>
)

export default Header
