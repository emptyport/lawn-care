import React from 'react'
import Banner from '../components/Banner'
import Services from '../components/Services'
import Pricing from '../components/Pricing'
import Contact from '../components/Contact'
import Footer from '../components/Footer'
import { graphql } from 'gatsby'

import Layout from '../components/layout'

const IndexPage = (props) => (
  <Layout>
    <Banner imgSrc={props.data.imageOne.childImageSharp.fluid} />
    <Services />
    <Pricing />
    <Contact />
    <Footer />
  </Layout>
)

export default IndexPage

export const pageQuery = graphql`
  query {
    imageOne: file(relativePath: { eq: "lawn_mower_darkened.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 2000) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
