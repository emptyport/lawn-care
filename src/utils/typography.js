import Typography from "typography"
import parnassusTheme from "typography-theme-parnassus"
parnassusTheme.baseFontSize = '18px';
parnassusTheme.bodyColor = 'white'

const typography = new Typography(parnassusTheme)

export default typography